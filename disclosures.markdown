---
layout: page
title: Disclosures
permalink: /disclosures/
---

All opinions—unless otherwise stated—are purely my own, and are not necessarily the opinion of Curtin University, the Curtin Student Guild, CoderDojo, CoderDojo WA, or The Fogarty Foundation.
  
I own shares in Telstra, Australia’s largest ISP.