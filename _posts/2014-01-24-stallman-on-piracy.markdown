---
layout: post
title:  "Stallman on Software Piracy"
date:   2014-01-24 13:13:13
categories: stallman
---

I asked Richard Stallman what’s more morally correct, pirating nonfree software, or supporting/paying for it.

> To “steal” non-free software would mean taking someone else’s copy away from him — for instance, stealing a disk. That is wrong.
>
> If you’re talking about making an unauthorized copy, that is not stealing. See [http://www.gnu.org/philosophy/words-to-avoid.html](http://www.gnu.org/philosophy/words-to-avoid.html). See [http://questioncopyright.org/files/minute_memes/cint/CINT_Nik_Theora_720.ogv](http://questioncopyright.org/files/minute_memes/cint/CINT_Nik_Theora_720.ogv).
>
> An unauthorized copy of a proprietary program is less bad than an authorized copy, but both of them trample your freedom (by assumption; if they respected your freedom, they’d be free software and your copy would be authorized).

Food for thought, if you subscribe to his ideology.