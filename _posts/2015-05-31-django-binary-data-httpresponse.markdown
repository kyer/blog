---
layout: post
title:  "Accessing binary data in a Django HttpRequest"
date:   2015-05-31
categories: coding
---
Django expects that the query strings it receives GET are going to be human-readable text. I'm making the mistake of writing a [torrent tracker in Django][beehive] and part of the [BitTorrent specification][bt-spec] involves a client passing urlencoded binary data to the tracker via a parameter in the GET query string. This is troublesome because Django automatically decodes the data it receives via GET. Whilst this isn't usually an issue because you're usually just dealing with text, it proves to be an issue when transferring binary data.

The fix is to [change][django-encoding] the `encoding` attribute of your `HttpRequest`. The `encoding` attribute describes what character encoding to use when automatically decoding HTTP responses. If we change from your project's default (usually `UTF-8`) to a more 'dumb' encoding like `ISO-8859-1` (where 1 byte = 1 character, and there are no invalid values), Django'll still automatically decode our binary data, but it'll be done using an encoding that'll allow us to easily reverse the damage.

Now for what you've been waiting for. Let's say my binary data was passed as a `GET` parameter called `info_hash`. You'd do this in your views function.

{% highlight python %}
# Switch the encoding used for automatic decoding to iso-8859-1
request.encoding = 'iso-8859-1'

# Save the binary data to a variable.
binary = request.GET['info_hash']

# Encode it again using iso-8859-1, which'll get you back the original data.
binary = binary.encode("iso-8859-1")

# Set encoding used for automatic decoding back to the project default, so future
# access to request data isn't impaired.
request.encoding = None
{% endhighlight %}

I expanded that for clarity, here's a nice short version:

{% highlight python %}
request.encoding = 'iso-8859-1'
binary = request.GET['info_hash'].binary.encode("iso-8859-1")
request.encoding = None
{% endhighlight %}

The `binary` variable now contains your data. 

This should fix your problem!

[beehive]: https://bitbucket.org/kyer/beehive
[bt-spec]: https://wiki.theory.org/BitTorrentSpecification#Tracker_Request_Parameters
[django-encoding]: https://docs.djangoproject.com/en/1.8/ref/request-response/#django.http.HttpRequest.encoding