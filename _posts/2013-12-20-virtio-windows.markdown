---
layout: post
title:  "Installing VirtIO drivers on Windows"
date:   2013-12-20 13:13:13
categories: virtualisation
---

Installing VirtIO drivers on Windows is non-trivial at best. The first problem you'll hit is that only the source and unsigned binaries are made available on the VirtIO site, and versions of Windows flat-out deny the use of unsigned drivers. The second issue is that Windows doesn’t let you install a driver unless there’s a piece of hardware that requires it; how is Windows going to boot from a VirtIO disk if it doesn’t have the driver?

To overcome this, you could slipstream the VirtIO drivers into the install ISO, or somehow shoehorn the driver into an already-running OS. These both look pretty unattractive. This said, I’m going to show you how to do the second one, because—like me—you thought that getting VirtIO working would be *easy*, and you've already gone ahead and configured the OS so it's too late for an ISO slipstream. Good for you!

Issue 1: unsigned drivers
=========================

The great guys at Red Hat have [compiled and signed the drivers][signed-drivers] for us. They conveniently come in an ISO for us to mount on our virtual machine with virsh:

    virsh # attach-disk vm1 images/virtio-drivers.iso hdc --type cdrom --mode readonly

Replace `vm1` with the name of your virtual machine, and `images/virtio-drivers.iso` with the path to your newly downloaded VirtIO drivers ISO file.

If you aren't using `virsh`, you're own your own. You should know how to mount an ISO on your guest anyway.

A note for Ubuntu users
=======================

AppArmor was extremely strict on my VM setup, so you may have to change your VM’s AppArmor profile. If you're having issues mounting teh ISO, this is probably your issue!

First, we need our VM’s UUID:

    $ egrep "DENIED" /var/log/syslog

You should see libvirt- a few times, followed by a UUID (e.g. `libvirt-004c9d52-8e04-6580-038f-c46991ead293`). Grab that string, it’s important.

Now, edit the AppArmor rules for your VM. Replace the UUID in the path below with your own.

    $ sudo vim /etc/apparmor.d/libvirt/libvirt-004c9d52-8e04-6580-038f-c46991ead293

Add a line, before the closing brace of the profile stanza, telling AppArmor to allow access to your ISO files. For example, if your ISO is stored in `/home/kye/iso-images/`, you could either allow access to JUST the ISO file with:

    /home/kye/iso-images/libvirt-drivers.iso r,

 the entire directory...

    /home/kye/iso-images/* r,

or the entire directory, and all sub-directories

    /home/kye/iso-images/** r,

Now tell AppArmor to read the new rule file.

    $ sudo apparmor_parser -r /etc/apparmor.d/libvirt/libvirt-004c9d52-8e04-6580-038f-c46991ead293

And, unfortunately, you’re going to have to restart your VM. Shut down from inside Windows to make sure nothing breaks, and then:

    $ virsh start vm1

You should now be able to run the `attach-disk` command from before.

Issue 2: forcing the driver installation
===============================

Now have to somehow tell Windows about the existence of VirtIO whilst still allowing it to boot. We can’t just change the interface used by the boot disk because that means we can’t boot to install the driver! The solution, as ugly as it is, is a second virtual disk! We create a ‘dummy’ disk that uses a VirtIO interface, and install the VirtIO drivers when Windows asks how to install the new hardware. We then change the boot disk’s interface to VirtIO, restart, and Windows automatically detects the change and sets itself up accordingly.

Create a sparse file to use as the virtual disk container:

    $ dd if=/dev/zero of=~/images/sparse.img bs=1M seek=4096 count=0

Now to tell virsh to mount the new disk the next time the VM is restarted. We need to edit your VM’s XML configuration file

    $ virsh edit vm1

This should open your text editor of choice. Scroll down until you find your `<disk>` blocks, and add this to the end:


{% highlight xml %}
    <disk type='block' device='disk'>
        <driver name='qemu' type='raw'/>
        <source dev='/home/kye/images/sparse.img'/>
        <target dev='hdb' bus='virtio'/>
    </disk>
{% endhighlight %}

Change the `dev='/home/kye/images/sparse.img'` section to wherever you made your sparse file. If another disk already exists with the `hdb` device name, increment the last letter (e.g. `hdc`, `hdd`, `hde`).

While you’re at it, you might as well use VirtIO for the networking interface. Scroll down until you find either `<interface type='bridge'>` or `<interface type='network'>`. Change the model to `virtio`, and delete the address definition. You’ll end up with something like this:

{% highlight xml %}
    <interface type='bridge'>
        <mac address='52:54:00:4b:73:f5'/>
        <source bridge='br0'/>
        <model type='virtio'/>
    </interface>
{% endhighlight %}

Now restart your virtual machine. Be warned, networking isn’t going to work, so you’ll have to VNC in.

Windows should see the devices automatically and ask for drivers. If it doesn’t, go into Device Manager and apply the drivers yourself. You don’t need to format the new disk once you’ve installed the driver, just make sure you can see it inside Disk Utility. Networking should work automatically once you install the driver.

Once you’ve confirmed this, `virsh edit vm1` again, and alter your boot disk’s configuration to use VirtIO. In my case, it looks like this:

{% highlight xml %}
    <disk type='block' device='disk'>
        <driver name='qemu' type='raw'/>
        <source dev='/dev/vm/vm1'/>
        <target dev='hda' bus='virtio'/>
    </disk>
{% endhighlight %}

If your old disk configuration had any other elements (e.g. `<address>`) just delete them. VirtIO will autoconfigure the rest for you, this should be all you need.

Also, you’re safe to remove the sparse disk configuration, and of course you can delete the sparse.img file.

Now save, restart your virtual machine, and—if it boots—you’ve finished!

[signed-drivers]: https://alt.fedoraproject.org/pub/alt/virtio-win/latest/images/virtio-win-0.1-74.iso