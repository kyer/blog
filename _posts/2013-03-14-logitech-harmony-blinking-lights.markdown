---
layout: post
title:  "Fix the ‘blinking lights’ problem on Logitech Harmony remotes"
date:   2013-12-20 13:13:13
categories: virtualisation
---

I love my Logitech Harmony 200 remotes. That said, a caveat of the remotes is that they sometimes go into a ‘Safe Mode’, especially when they’re running low on batteries. When the remote is in safe mode, the device selection lights will flash repeatedly, so if your remote is doing that, it’s probably in safe mode. The good news is that there's a fix, and it takes less than a minute!

  * Remove a battery from the remote.
  * Press and hold the ‘2’ button (on the pad of numbers, 0-9).
  * Whilst you’re still holding the ‘2’ button, insert the battery.
  * Keep holding the ‘2’ button for a few seconds, just for good measure.
  * Whilst you have the 2 button held down, the lights will stay lit. Give the remote a second or two to reset after they turn off, and it should work.

If the remote isn’t working, but the lights have stopped blinking by themselves, the remote’s memory may have been wiped. Just go back to MyHarmony and reconfigure the remote from your account, it wont take more than five minutes.

[logitech-sell-harmony]: http://www.techzone360.com/topics/techzone/articles/2013/03/01/328895-re-focus-mobile-accessories-causes-layoffs-logitech.htm