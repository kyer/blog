---
layout: post
title:  "Django on Mac OS X Cheatsheet"
date:   2015-01-27 13:13:13
categories: coding
---
After following these instructions, you’ll have a functional and ‘out of your way’ Django install on OS X. We’ll use virtualenv which allows you to create local Python environments, allowing you to mess up your new project’s development environment with all sorts of sketchy packages without messing up your system’s Python installation.

This isn’t really informative or anything. If you’re already good with *nix and Django and just want a good environment set up, this is for you. If else, probably look [somewhere else][django-install-tutorial].

First, you install [Homebrew][homebrew], a package manager for OS X:

    $ ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`

Then you should update Homebrew and install Python…

    $ brew update; brew install python

You will automatically get `pip` (a Python package installer) as part of the Python package. Use pip to install virtualenv.

    $ pip install virtualenv

Now create a virtualenv for your project and activate it.


    $ mkdir -p ~/code/coolsite
    $ cd ~/code/coolsite
    $ virtualenv .
    $ source bin/activate

From here, you can install Django:

    (coolsite)$ pip install django

If virtualenv has configured your environment correctly, you will automatically have Django’s management scripts in your path. So you can just use the following command to start a new Django app in the current directory.

    (coolsite)$ django-admin.py startproject coolsite

You should now have the following directory structure:

    coolsite/        # virtualenv root
    |- coolsite/     # django project root
    |  `- coolsite/  # django project files
    |- bin/          # binaries for your virtualenv
    |- inc/          # includes for your virtualenv
    `- lib/          # libraries for your virtualenv

As a minimum, this is all you need. `manage.py` is there for you to spin up applications:

    (coolsite)$ coolsite/manage.py startapp mycoolapp

and run the web server:

    (coolsite)$ coolsite/manage.py runserver

When you want to exit your virtualenv:

    (coolsite)$ deactivate
    $ echo "I'm free!"

But now you can’t start the web server!

    $ coolsite/manage.py runserver
    Traceback (most recent call last):
      File "./manage.py", line 8, in 
        from django.core.management import execute_from_command_line
    ImportError: No module named django.core.management

Why!? Because Django was only installed inside your virtualenv, which you exited! Let’s activate it again…

    $ source bin/activate
    (coolsite)$ coolsite/manage.py runserver
    Performing system checks...

If you want to make things a little nicer, you can use autoenv, a tool to automatically execute user-defined actions when you enter a certain directory. You can use this to automatically activate your virtualenv.

    (coolsite)$ brew install autoenv
    (coolsite)$ echo 'source /usr/local/opt/autoenv/activate.sh' >> ~/.bash_profile
    (coolsite)$ source ~/.bash_profile

autoenv is now installed. Now what?

    (coolsite)$ deactivate
    $ cd ~ 
    $ echo "source ~/code/coolsite/bin/activate" >> ~/code/coolsite/.env
    $ cd ~/code/coolsite
    (coolsite)$


Whenever you cd into your project’s directory, your virtualenv is automatically activated!

You can also use autoenv to do…anything when you open the project’s directory. For example, I use it to export other environment variables, which is useful for Django. That’s outside of the scope of this “tutorial”.

Have fun!


[homebrew]:                http://brew.sh/
[django-install-tutorial]: https://docs.djangoproject.com/en/1.7/intro/install/
