---
layout: post
title: "OS X's MobileBackups. What is it?"
date: 2013-02-12 13:13:13
categories: osx
---

So if you’re a Time Machine user on OS X Lion or higher, you’ve probably noticed a hidden directory in your primary hard drive’s root directory called `.MobileBackups`. This directory varies in size and it can sometimes get ridiculously big. Curious as to what is is? Want to remove it? You're in the right place.

I’ll start by saying that it isn’t mandatory that it stays there and you can quite easily get rid of it entirely without affecting your system at all. To do this, simply run this command:

    sudo tmutil disablelocal

If you care enough and want to know what it is, read on.

Local Backups are exactly that, a local (mobile) semi-replica of your Time Machine backups for use when you’re away from your Time Machine backup disk. Local Backups pretty much hook into the file deletion mechanisms of OS X. For you, this means that—instead of  the act of deleting a file actually removing (or should I say de-referencing) them from your hard-drive—it simply moves them to your `.MobileBackups` folder instead. This means that—if you’re away from your Time Machine backup disk—you can still restore edited or deleted files.

This said, the directory can get quite big when you’re editing or deleting some large files (for instance, the `.MobileBackups` folder on my 256GB SSD was 73GB before I disabled the feature).

It’s worth mentioning that Time Machine will disable or scale down this feature when your disk is getting too full, so—unless you’re like me and you like to keep your storage footprint as low as possible–it’s pretty safe to say that you’ll almost never have to disable this feature.
