---
layout: post
title:  "Interesting facts about Australian phone numbers"
date:   2015-11-26
categories: coding
---

I have been doing some stuff with Australian phone numbers at work lately. It turns out that Australia's phone numbering system (both current and historic) has great documentation provided by [ACMA][numbering-plan] and [Wikipedia wizards][wikipedia]. Here's a few of the less uninteresting facts.

- 1800 numbers usually take the form `1800 XXX XXX`, the only exception I can think of is `1800 REVERSE` (`1800 738 377 3`). I still can't really work out why this is even possible, but it breaks every phone number validation engine I have tried.

- There is also a seldom-used shorter 7-digit `1800` number format.

- The community service numbers still work, you can actually call `1196` and get the weather for free. What a time to be alive.

- `0` is actually Australia's internal trunk prefix, it isn't part of the area code. This is why a lot of our long-form numbers start with `0` (`089266926` -> `0 8 9266 9266`). This is also why you don't see the 0 when our phone numbers are formatted for international audiences. (e.g. `0413 131 313` vs `+61 413 131 313`).

- The location of geographic land-line numbers can be [identified][geo-number-list] using the first 3-4 digits.

- Australia doesn't forward the GSM emergency number (`112`) to `000` on landlines. Stop telling people to use it!

- ACMA has [fictitious numbers][fictitious-numbers] set aside for use in radio, books, film, and television.

- A bit of an old one, but there are a bunch of '[test numbers][test-numbers]' that read back your number, check your DSL codes, do automated callback.etc. PROTIP: the phone number formatting information on the linked site is incorrect.

[numbering-plan]: http://www.comlaw.gov.au/ComLaw/Legislation/LegislativeInstrumentCompilation1.nsf/0/7E0C74A05A3F12D4CA2573CA00220C09/$file/TelecomNumberingPlan1997Vol1.pdf

[fictitious-numbers]: http://www.acma.gov.au/Citizen/Consumer-info/All-about-numbers/Special-numbers/fictitious-numbers-for-radio-film-and-television-i-acma

[wikipedia]: https://en.wikipedia.org/wiki/Telephone_numbers_in_Australia

[test-numbers]: http://www.yabba.net.au/Support/Telcodes.htm

[geo-number-list]: https://en.wikipedia.org/wiki/Telephone_numbers_in_Australia#Geographic_numbers_2
