---
layout: page
title: About
permalink: /about/
---

I’m Kye. I study and work at [Curtin University](http://curtin.edu.au) in Perth, Western Australia. I am currently undertaking a bachelor of science in software engineering. I like computers, music, teaching, and psychology. I’m an Apple and Linux guy. I’m a big fan of Python and Django at the moment, but I’m guilty of language hopping.

I coordinate the [CoderDojo](http://coderdojo.org) at my university. CoderDojo is an open source, volunteer led, global movement of free coding clubs for young kids. Curtin University’s CoderDojo caters to over 160 kids a year, allowing over 30 volunteers from the community to introduce children of all ages to programming.

In addition to this, I work at Curtin University as part of their [Engineering Outreach](https://engineering.curtin.edu.au/outreach/) initiative. I design, coordinate, and deliver high-quality and interactive academic workshops for primary and secondary school students, with the aim of fostering their interest in [STEM fields](https://en.wikipedia.org/wiki/STEM_fields).

I am the Vice President of [ComSSA](https://comssa.org.au), Curtin’s computer science students association. I also sit on various [Curtin Student Guild](http://guild.curtin.edu.au/) boards and committees.

I’m on [Twitter](http://twitter.com/kyerussell) and [Bitbucket](http://bitbucket.org/kyer). Check out my code! I'm keen to collaborate, and I'm keen for any work.